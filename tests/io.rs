#![no_std]
#![no_main]

use core::panic::PanicInfo;
use Diva::{QemuExitCode, exit_qemu, serial_println, serial_print, println};


#[no_mangle]
pub extern "C" fn _start() -> ! {
    println_test();
    serial_println!("[ok]");
    exit_qemu(QemuExitCode::Success);
    loop {}
}

fn println_test() {
    serial_print!("DADB test...");
    println!("Test println");
}

#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    serial_println!("[failed]\n{}", _info);
    exit_qemu(QemuExitCode::Failed);
    loop {}
}