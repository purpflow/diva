use bootloader::bootinfo::MemoryMap;
use x86_64::VirtAddr;

pub mod allocator;
mod paging;


pub fn init(phys_mem_offset_u64: u64, memory_map: &'static MemoryMap) {
    let phys_mem_offset = VirtAddr::new(phys_mem_offset_u64);
    let mut mapper = unsafe { paging::init(phys_mem_offset) };
    let mut frame_allocator = unsafe {
        paging::BootInfoFrameAllocator::init(memory_map)
    };
    allocator::init_heap(&mut mapper, &mut frame_allocator)
        .expect("heap initialization failed");
}

#[test_case]
fn allocation() {
    use alloc::boxed::Box;
    let heap_value_1 = Box::new(41);
    let heap_value_2 = Box::new(13);
    assert_eq!(*heap_value_1, 41);
    assert_eq!(*heap_value_2, 13);
}
