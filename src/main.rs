#![allow(non_snake_case)]

#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(Diva::test_runner)]
#![reexport_test_harness_main = "test_main"]

const K_VERSION: &str = env!("CARGO_PKG_VERSION");
const K_NAME: &str = env!("CARGO_PKG_NAME");

use core::panic::PanicInfo;
use alloc::{boxed::Box, vec, vec::Vec, rc::Rc};
use bootloader::{BootInfo, entry_point};
use Diva::{cpu, memory, print, println};


extern crate alloc;

entry_point!(k_main);

#[no_mangle]
fn k_main(boot_info: &'static BootInfo) -> ! {
    println!("{} v{}", K_NAME, K_VERSION);

    print!("Init... ");
    Diva::init(boot_info);
    println!("[ok]");


    #[cfg(test)]
    test_main();

    Diva::hlt_loop();
}

#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    println!("{}", info);
    Diva::hlt_loop();
}

#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    Diva::test_panic_handler(info)
}
