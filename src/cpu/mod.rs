mod interrupts;
mod gdt;


pub fn init() {
    interrupts::init();
    gdt::init();
}


#[test_case]
fn test_breakpoint_exception() {
    x86_64::instructions::interrupts::int3();
}
